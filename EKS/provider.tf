provider "aws" {
   profile    = "default"
   region     = "us-east-1"
   alias      = "AWS"
}

provider "kubernetes" {
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", aws_eks_cluster.aws_eks.name]
    command     = "aws"
  }
  alias = "K8S"
}
