resource "aws_iam_role" "eks_cluster" {
  name = "eks-cluster"
  provider = aws.AWS
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  provider   = aws.AWS
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_cluster.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicy" {
  provider   = aws.AWS
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks_cluster.name
}

resource "aws_eks_cluster" "aws_eks" {
  provider = aws.AWS
  name     = "eks_cluster_natura"
  role_arn = aws_iam_role.eks_cluster.arn

  vpc_config {
    subnet_ids = ["subnet-4079244f", "subnet-3e462010"]
  }

  tags = {
    Name = "EKS_natura"
  }
}

resource "aws_iam_role" "eks_nodes" {
  provider = aws.AWS
  name = "eks-node-group-natura"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  provider   = aws.AWS
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  provider   = aws.AWS
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  provider   = aws.AWS
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_nodes.name
}

resource "aws_eks_node_group" "node" {
  provider        = aws.AWS
  cluster_name    = aws_eks_cluster.aws_eks.name
  node_group_name = "node_natura"
  node_role_arn   = aws_iam_role.eks_nodes.arn
  subnet_ids      = ["subnet-4079244f", "subnet-3e462010"]

  scaling_config {
    desired_size = 1
    max_size     = 1
    min_size     = 1
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]
}

/*resource "kubernetes_namespace" "prdnamespace" {
  provider = kubernetes.K8S
  metadata {
    annotations = {
      name = "prd"
    }

    name = "prd"
  }
  depends_on                     = [ aws_eks_node_group.node ]  
}

resource "kubernetes_namespace" "hmlnamespace" {
  provider = kubernetes.K8S
  metadata {
    annotations = {
      name = "hml"
    }

    name = "hml"
  }
  depends_on                     = [ aws_eks_node_group.node ]  
}

resource "kubernetes_namespace" "devnamespace" {
  provider = kubernetes.K8S
  metadata {
    annotations = {
      name = "dev"
    }

    name = "dev"
  }
  depends_on                     = [ aws_eks_node_group.node ]  
}*/

