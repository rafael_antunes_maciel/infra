data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn2-ami-hvm*",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

output "amazon_linux_ami" {
  value = data.aws_ami.amazon_linux.id
}

resource "aws_instance" "jenkins" {
  ami                  = data.aws_ami.amazon_linux.id
  instance_type        = "t2.micro"
  security_groups      = [aws_security_group.web_traffic.name]
  key_name             = "EC2-Support"
  iam_instance_profile = "Admin-EC2"

provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",	
      "sudo yum install -y java-1.8.0",
      "sudo yum remove -y java-1.7.0-openjdk",
      "sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo",	  
      "sudo rpm  -import https://pkg.jenkins.io/redhat/jenkins.io.key",
      "sudo yum install jenkins -y",
      "sudo service jenkins start",
      "sudo systemctl enable jenkins",
      "sudo yum install -y docker",
      "sudo service docker start",  
      "sudo systemctl enable docker",
      "sudo curl -LO \"https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl\"",
	  "sudo chmod 755 kubectl",
	  "sudo mv kubectl /bin/",
	  "sudo usermod -aG docker jenkins",
	  "curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3",
	  "chmod 700 get_helm.sh",
	  "sed -i 's#/usr/local/bin#/bin#' get_helm.sh",
      "sudo ./get_helm.sh",	  
    ]
  }

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ec2-user"
    private_key = file("/terraform/EC2-Support.pem")
  }

  tags = {
    "Name"      = "Jenkins_Server"
    "Terraform" = "true"
  }
depends_on = [ aws_security_group.web_traffic ]
}



