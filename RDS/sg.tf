// CREATE SECURITY GROUP Test

resource "aws_security_group" "RDSTest" {

  name = "RDS Test"
  vpc_id = "vpc-efff4895"
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] // For test
    description = "RDS Test"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "RDS Test"
  }
}
