resource "aws_db_instance" "default" {
  identifier             = "chucknorrismysql"
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  name                   = "mydb"
  username               = "admin"
  password               = "${var.password}"
  parameter_group_name   = "default.mysql5.7"
  vpc_security_group_ids = ["${aws_security_group.RDSTest.id}"]
  skip_final_snapshot    = "true"

  depends_on             = [ aws_security_group.RDSTest ]
}

variable "password" {
  //default = "password"
description = "production password"
}
