//Create S3 Bucket Prod

resource "aws_s3_bucket" "natura-prd" {  
  bucket   = "natura-prd-teste"
  acl      = "public-read"
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
  tags     = {
    Name        = "natura-prd-teste"
    Environment = "production"
  }
}

//Create S3 Bucket Dev

resource "aws_s3_bucket" "natura-dev" {  
  bucket   = "natura-dev-teste"
  acl      = "public-read"
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
  tags     = {
    Name        = "natura-dev-teste"
    Environment = "development"
  }
}

//Create S3 Bucket Hlg

resource "aws_s3_bucket" "natura-hml" {  
  bucket   = "natura-hml-teste"
  acl      = "public-read"
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
  tags     = {
    Name        = "natura-hml-teste"
    Environment = "homologation"
  }
}

